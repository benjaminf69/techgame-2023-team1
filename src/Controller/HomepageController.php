<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\PostsRepository;

class HomepageController extends AbstractController
{
    #[Route('/', name: 'app_landing')]
    public function index(): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_home');
        }
        return $this->render('homepage/landing.html.twig', [

        ]);
    }

    #[Route('/app/home', name: 'app_home')]
    public function home(PostsRepository $postsRepository): Response
    {

        $posts = $postsRepository->findAll();

        return $this->render('homepage/home.html.twig', [
            'controller_name' => 'HomepageController',
            'posts'=>$posts
        ]);
    }
}

