<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CategoriesRepository;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Entity\Images;

use App\Entity\Posts;
use App\Entity\Categories;
use App\Entity\User;
use App\Form\CoursType;

use Doctrine\ORM\EntityManagerInterface;


class CoursesController extends AbstractController
{
    #[Route('/courses', name: 'app_courses')]
    public function index(Request $request, EntityManagerInterface $entityManager, CategoriesRepository $categoryRepository): Response
    {

        $courses = $entityManager->getRepository(Posts::class)->findBy(['category' => '3']);

        if (!$courses) {
            throw $this->createNotFoundException(
                'No courses found'
            );
        }

        // FORMULAIRE POST
        /** @var User $user */
        $user = $this->getUser();

        if ($user) {
            if ($user->getStatus() == 'student') {
                return $this->redirectToRoute("app_courses");
            }
        } else {
            return $this->redirectToRoute("app_login");
        }

        /** @var \App\Entity\Posts $cours */
        $cours = new Posts();
        
        //$category = $entityManager->getRepository(Categories::class)->findOneBy(['name' => 'Cours']);
        $form = $this->createForm(CoursType::class, $cours);


        // Pré-remplir le champ "user" avec l'utilisateur connecté
        $user = $this->getUser();
        $form->get('user')->setData($user);
        $cours->setUser($user);

        // Pré-remplir le champ "category" avec la catégorie "Market"
        $coursCategory = $categoryRepository->findByName('Cours');
        $form->get('category')->setData($coursCategory);
        $cours->setCategory($coursCategory);

        $form->handleRequest($request);
    

        if ($form->isSubmitted() && $form->isValid()) {
            //$cours->setUser($user);
            //$cours->setCategory($category);

            // Gérer l'upload de l'image
            /** @var UploadedFile $imageFile */
            $imageFile = $form->get('imageFile')->getData();
                if ($imageFile) {
                    $image = new Images();
                    $image->setPost($cours);
                    $cours->addImage($image);
    
                    // Effectuez le téléchargement de l'image
                    $image->setImageFile($imageFile);
                }

            $entityManager->persist($cours);
            $entityManager->flush();

            return $this->redirectToRoute('app_courses');
        }
        //FIN FORMULAIRE POST

        return $this->render('courses/index.html.twig', [
            'courses' => $courses,
            'coursForm' => $form->createView(),
        ]);
    }


    #[Route('/courses/{id}', name: 'cours_show')]
    public function show(EntityManagerInterface $entityManager, int $id): Response
    {
        $cours = $entityManager->getRepository(Posts::class)->find($id);

        if (!$cours) {
            // throw $this->createNotFoundException(
            //     'No cours found for id ' . $id
            // );
            return $this->render('courses/cours/show.html.twig', ['cours' => ['title' => 'Erreur', 'content' => 'Aucun cours ne correspond à cet id']]);
        }


        return $this->render('courses/cours/show.html.twig', ['cours' => $cours]);
    }

    #[Route('/courses_new', name: 'cours_new')]
    public function new(Request $request, EntityManagerInterface $entityManager)
    {
        /** @var User $user */
        $user = $this->getUser();

        if ($user) {
            if ($user->getStatus() == 'student') {
                return $this->redirectToRoute("app_courses");
            }
        } else {
            return $this->redirectToRoute("app_login");
        }

        /** @var \App\Entity\Posts $cours */
        $cours = new Posts();
        
        $category = $entityManager->getRepository(Categories::class)->findOneBy(['name' => 'Cours']);

        $form = $this->createForm(CoursType::class, $cours);
        $form->handleRequest($request);
    

        if ($form->isSubmitted() && $form->isValid()) {
            $cours->setUser($user);
            $cours->setCategory($category);

            $entityManager->persist($cours);
            $entityManager->flush();

            return $this->redirectToRoute('app_courses');
        }

        return $this->render('courses/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
