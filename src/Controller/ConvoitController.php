<?php

namespace App\Controller;

use App\Entity\Categories;
use App\Entity\Posts;
use App\Repository\PostsRepository;
use App\Form\ConvoitFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CategoriesRepository;

class ConvoitController extends AbstractController
{
    private $entityManager;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->entityManager = $doctrine->getManager();
    }
    #[Route('/app/category/convoit', name: 'app_convoit')]
    public function index(PostsRepository $PostRepository, Request $request, EntityManagerInterface $entityManager, CategoriesRepository $categoryRepository): Response
    {
        $user = $this->getUser();

        $posts = $PostRepository->findByExampleField('Covoiturage');
        $category = $entityManager->getRepository(Categories::class)->findOneBy(['name' => 'Covoiturage']);

        $convoit = new Posts();
        $convoitForm = $this->createForm(ConvoitFormType::class, $convoit);


        // Pré-remplir le champ "user" avec l'utilisateur connecté
        $user = $this->getUser();
        $convoitForm->get('user')->setData($user);
        $convoit->setUser($user);
        
        // Pré-remplir le champ "category" avec la catégorie "Market"
        $coursCategory = $categoryRepository->findByName('Covoiturage');
        $convoitForm->get('category')->setData($coursCategory);
        $convoit->setCategory($coursCategory);

        $convoitForm->handleRequest($request);

        if ($convoitForm->isSubmitted() && $convoitForm->isValid()) {
            //$convoit->setUser($user);
            //$convoit->setCategory($category);

            $this->entityManager->persist($convoit);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_convoit');
        }
        return $this->render('convoit/index.html.twig', [
            'controller_name' => 'ConvoitController',
            'posts' => $posts,
            'convoitForm' => $convoitForm->createView()
        ]);
}}
