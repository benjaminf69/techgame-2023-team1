<?php

namespace App\Controller;

use App\Entity\Friendships;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TrombinoscopeController extends AbstractController
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly EntityManagerInterface $entityManager
    )
    {
    }

    #[Route('/app/trombinoscope', name: 'app_trombinoscope')]
    public function index(UserRepository $userRepository): Response
    {
        $users = $userRepository->findByExampleField('étudiant');
        return $this->render('trombinoscope/index.html.twig', [
            'controller_name' => 'TrombinoscopeController',
            'users' => $users
        ]);
    }

    /*
     * Create a new friendship with the user connected and the user clicked
     *
     */
     #[Route('/app/trombinoscope/{id}', name: 'app_trombinoscope_add')]
    public function addFriend(Request $request, int $id): Response
    {
        $userConnected = $this->getUser();
        $userClicked = $this->userRepository->find($id);
        $friendship = new Friendships();
        $friendship->setUser1($userConnected);
        $friendship->setUser2($userClicked);
        $friendship->setStatus('pending');
        $this->entityManager->persist($friendship);
        $this->entityManager->flush();
        $this->addFlash('success', 'Demande d\'ami envoyée');
        return $this->redirectToRoute('app_trombinoscope');
    }
}
