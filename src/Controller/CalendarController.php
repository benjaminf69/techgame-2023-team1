<?php

namespace App\Controller;

use App\Entity\Events;
use App\Form\EventFormType;
use App\Repository\EventsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CategoriesRepository;

class CalendarController extends AbstractController
{

    public function __construct(
        private readonly EventController $eventController, 
        private readonly EventsRepository $eventsRepository, 
        private readonly EntityManagerInterface $entityManager
    )
    {
    }

    #[Route('/app/calendar', name: 'app_calendar')]
    public function index(Request $request, CategoriesRepository $categoryRepository): Response
    {
        $events = $this->eventsRepository->findAll();
        $event = new Events;
        $formEvent = $this->createForm(EventFormType::class, $event);

        // Pré-remplir le champ "user" avec l'utilisateur connecté
        $user = $this->getUser();
        $formEvent->get('user')->setData($user);
        $event->setUser($user);
        
        // Pré-remplir le champ "category" avec la catégorie "Market"
        $coursCategory = $categoryRepository->findByName('Evènement');
        $formEvent->get('category')->setData($coursCategory);
        $event->setCategory($coursCategory);

        $formEvent->handleRequest($request);

        if ($formEvent->isSubmitted() && $formEvent->isValid()) {
            $this->entityManager->persist($event);
            $this->entityManager->flush();
            return $this->redirectToRoute('app_calendar'); // utilisez le nom de la route
        }

        $evt = [];
        foreach ($events as $event) {
            $evt[] = [
                'id' => $event->getId(),
                'title' => $event->getName(),
                'start' => $event->getStartDate()->format('Y-m-d H:i:s'),
                'end'=> $event->getEndDate()->format('Y-m-d H:i:s'),
                'description'=> $event->getDescription(),
                'category' => [
                    'id' => $event->getCategory()->getId(),
                    'name' => $event->getCategory()->getName(),
                ],
                'user' => [
                    'id' => $event->getUser()->getId(),
                    'lastname' => $event->getUser()->getLastName(),
                ],
                'location' => $event->getLocation(),
            ];
        }

        $data = json_encode($evt);

        return $this->render('calendar/index.html.twig', [
            'data' => $data,
            'eventForm' => $formEvent->createView(),
        ]);
    }
}
