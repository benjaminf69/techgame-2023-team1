<?php

namespace App\Controller;

use App\Entity\Friendships;
use App\Entity\User;
use App\Form\AccountFormType;
use App\Repository\FriendshipsRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly FriendshipsRepository $friendshipRepository,
        private readonly UserRepository $userRepository
    )
    {
    }

    #[Route('/app/account', name: 'app_account')]
    public function index(Request $request, Security $security, EntityManagerInterface $entityManager, UserPasswordHasherInterface $userPasswordHasher): Response
    {
        $user = $security->getUser();
        $form = $this->createForm(AccountFormType::class, $user);
        $AllRelations = $this->friendshipRepository->getPendingRequests($user);
        $currentPassword = $request->request->get('currentPassword');
        $form->handleRequest($request);
        $userConnected = $this->getUser();
        $ContactList = $this->friendshipRepository->getContactList($userConnected);
        $contactList = [];
        $friendsRequests = [];
       // get only user2 in the friendrequest array
        foreach ($AllRelations as $friendRequest) {
            if ($friendRequest->getUser1() === $userConnected && $friendRequest->getStatus() === 'pending') {
                $friendsRequests[] = [
                    'id' => $friendRequest->getUser2()->getId(),
                    'name' => $friendRequest->getUser2()->getFirstName(),
                    'email' => $friendRequest->getUser2()->getEmail(),
                    'status' => $friendRequest->getStatus(),
                    'initiator' => $friendRequest->getUser1()->getId(),
                ];
            }
            if ($friendRequest->getUser2() === $userConnected && $friendRequest->getStatus() === 'pending') {
                $friendsRequests[] = [
                    'id' => $friendRequest->getUser1()->getId(),
                    'name' => $friendRequest->getUser1()->getFirstName(),
                    'email' => $friendRequest->getUser1()->getEmail(),
                    'status' => $friendRequest->getStatus(),
                    'initiator' => $friendRequest->getUser1()->getId(),
                ];
            }
        }
        foreach ($ContactList as $contact) {
            if ($contact->getUser1() === $userConnected && $contact->getStatus() === 'accepted') {
                $contactList[] = [
                    'id' => $contact->getUser2()->getId(),
                    'email' => $contact->getUser2()->getEmail(),
                    'name' => $contact->getUser2()->getFirstName(),
                    'status' => $contact->getStatus(),
                ];
            } elseif ($contact->getUser2() === $userConnected && $contact->getStatus() === 'accepted') {
                $contactList[] = [
                    'id' => $contact->getUser1()->getId(),
                    'email' => $contact->getUser1()->getEmail(),
                    'name' => $contact->getUser1()->getFirstName(),
                    'status' => $contact->getStatus(),
                ];
            }
        }
        if ($form->isSubmitted() && $form->isValid()) {
            // Sauvegardez les modifications dans la base de données
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('password')->getData()
                )
            );   

            $entityManager->persist($user);
            $entityManager->flush();

            // Redirigez l'utilisateur vers une autre page ou affichez un message de réussite
            $this->addFlash('success', 'Modifications sauvegardées avec succès');
        }
        return $this->render('account/account.html.twig', [
            'user' => $user,
            'friendRequests' => $friendsRequests,
            'contactList' => $contactList,
            'accountForm' => $form->createView(),
        ]);
    }
    #[Route('/app/account/accept/{id}', name: 'app_account_accept')]
    public function acceptFriend(Request $request, int $id): Response
    {
        $userConnected = $this->getUser();
        $userClicked = $this->userRepository->find($id);
        $legacyFriendship = $this->friendshipRepository->findOneBy(['user1' => $userClicked, 'user2' => $userConnected]);
        $newFriendship = new Friendships();
        $newFriendship->setUser1($userConnected);
        $newFriendship->setUser2($userClicked);
        $newFriendship->setStatus('accepted');
        $this->entityManager->persist($newFriendship);
        $this->entityManager->flush();
        $this->addFlash('success', 'Demande d\'ami acceptée');
        // I need to delete the legacy friendship
        $this->entityManager->remove($legacyFriendship);
        $this->entityManager->flush();
        return $this->redirectToRoute('app_account');
    }
    #[Route('/app/account/refuse/{id}', name: 'app_account_refuse')]
    public function refuseFriend(Request $request, int $id): Response
    {
        // Delete the friendship
        $userConnected = $this->getUser();
        $userClicked = $this->userRepository->find($id);
        $friendship = $this->friendshipRepository->findOneBy(['user1' => $userClicked, 'user2' => $userConnected]);
        $this->entityManager->remove($friendship);
        $this->entityManager->flush();
        $this->addFlash('error', 'Demande ami refusé');
        return $this->redirectToRoute('app_account');
    }
    #[Route('/app/account/remove/{id}', name: 'app_account_remove')]
    public function removeFriend(Request $request, int $id): Response
    {
        $userConnected = $this->getUser();
        $userClicked = $this->userRepository->find($id);
        $friendship = $this->friendshipRepository->findOneBy(['user1' => $userConnected, 'user2' => $userClicked]);
        $this->entityManager->remove($friendship);
        $this->entityManager->flush();
        $this->addFlash('error', 'Ami supprimé');
        return $this->redirectToRoute('app_account');
    }
}
