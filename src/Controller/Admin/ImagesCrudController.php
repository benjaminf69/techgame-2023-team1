<?php

namespace App\Controller\Admin;

use App\Entity\Images;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ImagesCrudController extends AbstractCrudController
{

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Images')
            ->setEntityLabelInPlural('Images')
            ->setPageTitle(Crud::PAGE_NEW, 'Création d\'une image');
    }
    public static function getEntityFqcn(): string
    {
        return Images::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')->hideOnIndex()->hideOnForm();
        yield AssociationField::new('post',"Post");
        yield TextField::new('imageFile', 'Choisir une image')->setFormType(VichImageType::class)->hideOnIndex();
        yield ImageField::new('imagePath', 'Image')->setBasePath('/images')->onlyOnIndex();
        yield DateTimeField::new('createdAt', "Créé le")->setFormTypeOption('disabled', true);
        return [

        ];
    }
    
}
