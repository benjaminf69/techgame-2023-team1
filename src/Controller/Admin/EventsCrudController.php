<?php

namespace App\Controller\Admin;

use App\Entity\Events;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class EventsCrudController extends AbstractCrudController
{

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Evènement')
            ->setEntityLabelInPlural('Evènements')
            ->setPageTitle(Crud::PAGE_NEW, 'Création d\'un évènement');
    }
    public static function getEntityFqcn(): string
    {
        return Events::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')->hideOnIndex()->hideOnForm();
        yield AssociationField::new('user',"Créer par");
        yield AssociationField::new('category',"Catégorie");
        yield TextField::new('name',"Nom");
        yield TextField::new('description',"Description");
        yield TextField::new('location',"Emplacement");
        yield DateTimeField::new("startDate","Débute le");
        yield DateTimeField::new("endDate","Fin le");
        
        return [

        ];
    }
    
}
