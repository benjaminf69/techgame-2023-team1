<?php

namespace App\Controller\Admin;

use App\Entity\Notifications;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class NotificationsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Notifications::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnIndex()->hideOnForm(),
            AssociationField::new('user',"Utilisateur"),
            TextField::new('link', 'Lien'),
            TextEditorField::new('message', 'Message'),
            DateTimeField::new('readAt', 'Lu le')->hideWhenCreating()->SetDisabled(true),
            DateTimeField::new('createdAt', 'Créer le')->SetDisabled(true),
        ];
    }

}
