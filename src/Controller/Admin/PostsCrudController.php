<?php

namespace App\Controller\Admin;

use App\Entity\Posts;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class PostsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Posts::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnIndex()->hideOnForm(),
            AssociationField::new('user',"Utilisateur"),
            AssociationField::new('category',"Catégorie"),
            TextField::new('title', 'Titre'),
            TextEditorField::new('content', 'Description'),
            TextField::new('extraField', 'Champ supplémentaire'),
            AssociationField::new('images',"Images"),
            DateTimeField::new('createdAt', 'Créer le')->SetDisabled(true),
            DateTimeField::new('updatedAt', 'Modifier le')->hideOnForm(),


        ];
    }


    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('edit', 'Modifier le post')
            ->setPageTitle('new', 'Créer un post')
            // Autres configurations éventuelles
            ;
    }
}
