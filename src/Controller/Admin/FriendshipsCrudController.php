<?php

namespace App\Controller\Admin;

use App\Entity\Friendships;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class FriendshipsCrudController extends AbstractCrudController
{


    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Demande d\'amis')
            ->setEntityLabelInPlural('Demande d\'amis')
            ->setPageTitle(Crud::PAGE_NEW, 'Demande d\'amis');
    }
    public static function getEntityFqcn(): string
    {
        return Friendships::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new('user1',"Invitation envoyé par");
        yield AssociationField::new('user2',"Invitation reçu à");
        yield IdField::new('id')->hideOnIndex()->hideOnForm();
        yield ChoiceField::new("status")->setChoices([
            'En attente' => 'en_attente',
            'Acceptée' => 'acceptee',
            'Refusée' => 'refusee',
        ]);
        yield DateTimeField::new('createdAt', "Créé le")->setFormTypeOption('disabled', true);
        yield DateTimeField::new("updatedAt","Modifié le")->hideOnForm();

        return [

        ];
    }
    
}
