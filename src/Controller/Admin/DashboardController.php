<?php

namespace App\Controller\Admin;

use App\Entity\Categories;
use App\Entity\Comments;
use App\Entity\Events;
use App\Entity\Friendships;
use App\Entity\Images;
use App\Entity\Message;
use App\Entity\Notifications;
use App\Entity\Posts;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
         $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
         return $this->redirect($adminUrlGenerator->setController(UserCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('School Social G4')
            ->renderContentMaximized()
            ->renderSidebarMinimized()
            ->setFaviconPath('favicon.ico');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::section('Main');
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');

        yield MenuItem::section('User Management');
        yield MenuItem::linkToCrud('Users', 'fas fa-users', User::class);
        yield MenuItem::linkToCrud('Friendships', 'fas fa-user-friends', Friendships::class);

        yield MenuItem::section('Content Management');
        yield MenuItem::linkToCrud('Posts', 'fas fa-pen-square', Posts::class);
        yield MenuItem::linkToCrud('Categories', 'fas fa-tags', Categories::class);
        yield MenuItem::linkToCrud('Images', 'fas fa-image', Images::class);

        yield MenuItem::section('Notifications');
        yield MenuItem::linkToCrud('Notifications', 'fas fa-bell', Notifications::class);

        yield MenuItem::section('Events');
        yield MenuItem::linkToCrud('Events', 'fas fa-calendar-alt', Events::class);

        yield MenuItem::section('Comments');
        yield MenuItem::linkToCrud('Comments', 'far fa-comment', Comments::class);

    }

}
