<?php

namespace App\Controller;

use App\Repository\FriendshipsRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly FriendshipsRepository $friendshipRepository
    )
    {
    }

    #[Route('/app/contact', name: 'app_contact')]
    public function index(): Response
    {
        $userConnected = $this->getUser();
        $ContactList = $this->friendshipRepository->getContactList($userConnected);
        $contactList = [];

        foreach ($ContactList as $contact) {
            if ($contact->getUser1() === $userConnected && $contact->getStatus() === 'accepted') {
                $contactList[] = [
                    'id' => $contact->getUser2()->getId(),
                    'name' => $contact->getUser2()->getFirstName(),
                    'status' => $contact->getStatus(),
                ];
            } elseif ($contact->getUser2() === $userConnected && $contact->getStatus() === 'accepted') {
                $contactList[] = [
                    'id' => $contact->getUser1()->getId(),
                    'name' => $contact->getUser1()->getFirstName(),
                    'status' => $contact->getStatus(),
                ];
            }
        }

        return $this->render('contact/index.html.twig', [
            'contactList' => $contactList,
            'userConnected' => $userConnected,
        ]);
    }
}
