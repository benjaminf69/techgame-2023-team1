<?php

namespace App\Controller;

use App\Entity\Posts;
use App\Form\MarketFormType;
use App\Repository\CategoriesRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Images;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class MarketController extends AbstractController
{
    private $entityManager;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->entityManager = $doctrine->getManager();
        
    }

    #[Route('/app/market', name: 'app_market')]
    public function index(Request $request,  CategoriesRepository $categoryRepository): Response
    {
        $repository = $this->entityManager->getRepository(Posts::class);
        $donnees = $repository->createQueryBuilder('p')
            ->innerJoin('p.category', 'category')
            ->where('category.name = :nomCategorie')
            ->setParameter('nomCategorie', 'Market')
            ->getQuery()
            ->getResult();

        $post = new Posts();
        $marketForm = $this->createForm(MarketFormType::class, $post);

        // Pré-remplir le champ "user" avec l'utilisateur connecté
        $user = $this->getUser();
        $marketForm->get('user')->setData($user);
        $post->setUser($user);


        // Pré-remplir le champ "category" avec la catégorie "Market"
        $marketCategory = $categoryRepository->findByName('Market');
        $marketForm->get('category')->setData($marketCategory);
        $post->setCategory($marketCategory);



        $marketForm->handleRequest($request);

        if ($marketForm->isSubmitted() && $marketForm->isValid()) {

            // Gérer l'upload de l'image
            /** @var UploadedFile $imageFile */
            $imageFile = $marketForm->get('imageFile')->getData();
                if ($imageFile) {
                    $image = new Images();
                    $image->setPost($post);
                    $post->addImage($image);
    
                    // Effectuez le téléchargement de l'image
                    $image->setImageFile($imageFile);
                }
            $this->entityManager->persist($post);
            $this->entityManager->flush();

            // Rediriger vers la route 'app_market' après l'ajout
            return $this->redirectToRoute('app_market');
        }

        return $this->render('market/index.html.twig', [
            'controller_name' => 'MarketController',
            'donnees' => $donnees,
            'marketForm' => $marketForm->createView(),
        ]);
    }
}