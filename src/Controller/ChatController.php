<?php

namespace App\Controller;

use App\Entity\Message;
use App\Entity\User;
use App\Repository\MessageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\PublisherInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Routing\Annotation\Route;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

class ChatController extends AbstractController implements MessageComponentInterface
{
    private $connections;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly MessageRepository $messageRepository
    )
    {
        $this->connections = new \SplObjectStorage();
    }

    #[Route('/chat', name: 'chat')]
    public function index(): Response
    {
        return $this->render('chat/index.html.twig', [
            'controller_name' => 'ChatController',
            'senderName' => $this->getUser()->getFirstName(),
        ]);
    }

    public function onOpen(ConnectionInterface $conn): void
    {
        $this->connections->attach($conn);
    }

    public function onMessage(ConnectionInterface $from, $msg): void
    {
        foreach ($this->connections as $conn) {
            if ($conn !== $from) {
                $conn->send($msg);
            }
        }
    }

    public function onClose(ConnectionInterface $conn): void
    {
        $this->connections->detach($conn);
    }

    public function onError(ConnectionInterface $conn, \Exception $e): void
    {
        $conn->close();
    }

    // Use the Mercure hub to publish updates that will be pushed to the client-side through a WebSocket
    #[Route('api/message/send/{senderId}/{receiverId}', name: 'message_send', methods: ['POST'])]
    public function sendMessage(Request $request, PublisherInterface $publisher, $senderId, $receiverId): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $message = new Message();
        $message->setContent($data['content']);
        $message->setSender($this->entityManager->getRepository(User::class)->find($senderId));
        $message->setReceiver($this->entityManager->getRepository(User::class)->find($receiverId));
        $receiverId = $data['receiver_id'];
        $this->entityManager->persist($message);
        $this->entityManager->flush();
        $update = new Update(
            'http://localhost:8000/api/messages/' . $senderId . '/' . $receiverId,
            json_encode([
                'id' => $message->getId(),
                'content' => $message->getContent(),
                'sender_id' => $message->getSender()->getId(),
                'receiver_id' => $receiverId,
            ])
        );
        $publisher($update);
        return new JsonResponse(['status' => 'Message sent!'], Response::HTTP_CREATED);
    }

    #[Route('/api/messages/{senderId}/{receiverId}', name: 'message_receive', methods: ['GET'])]
    public function receiveMessage($senderId, $receiverId): JsonResponse
    {
        $messages = $this->entityManager->getRepository(Message::class)->findMessagesBetweenUsers(
            $this->entityManager->getRepository(User::class)->find($senderId),
            $this->entityManager->getRepository(User::class)->find($receiverId)
        );
        $messagesData = [];
        foreach ($messages as $message) {
            $messagesData[] = [
                'id' => $message->getId(),
                'content' => $message->getContent(),
                'sender_id' => $message->getSender()->getId(),
                'receiver_id' => $message->getReceiver()->getId(),
            ];
        }
        return new JsonResponse($messagesData);
    }

}
