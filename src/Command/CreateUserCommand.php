<?php

namespace App\Command;

use Symfony\Bundle\SecurityBundle\Security\UserAuthenticator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use Faker\Factory;
class CreateUserCommand extends Command
{
    ## To add a new user, run this command:
    ## php bin/console app:add-user <email> <password>
    public function __construct(
        private readonly UserPasswordHasherInterface $passwordEncoder,
        private readonly EntityManagerInterface $entityManager
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('app:add-user')
            ->setDescription('Add a new user with a hashed password')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $faker = Factory::create();
        $io = new SymfonyStyle($input, $output);

        // Création de plusieurs utilisateurs
        for ($i = 0; $i < 10; $i++) {
            $email = $faker->unique()->safeEmail;
            $password = $faker->password;
            $user = new User();
            $user->setEmail($email);
            $user->setPassword($this->passwordEncoder->hashPassword($user, $password));
            $user->setRoles(['ROLE_USER']);
            $user->setFirstName($faker->firstName);
            $user->setLastName($faker->lastName);
            $user->setIsBDE($faker->boolean);
            $user->setStatus($faker->randomElement(['étudiant']));
            $this->entityManager->persist($user);
        }
        $this->entityManager->flush();
        $io->success('10 utilisateurs ajoutés avec succès.');

        return Command::SUCCESS;
    }
}

