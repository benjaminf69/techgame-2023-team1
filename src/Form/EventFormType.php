<?php

namespace App\Form;

use App\Entity\Events;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;

class EventFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class)
            ->add('description', TextType::class)
            ->add('startDate', DateTimeType::class, [ // Use DateTimeType
                'date_widget' => 'single_text', // Display the date input as a single text field
                'time_widget' => 'single_text', // Display the time input as a single text field
                'constraints' => [
                    new GreaterThanOrEqual('today'),
                ],
            ])
            ->add('endDate', DateTimeType::class, [ // Use DateTimeType
                'date_widget' => 'single_text', // Display the date input as a single text field
                'time_widget' => 'single_text', // Display the time input as a single text field
                'constraints' => [
                    new GreaterThanOrEqual('today'),
                ],
            ])
            ->add('location', TextType::class)
            ->add('user', TextType::class, [
                'disabled' => true,
            ])
            ->add('category', TextType::class, [
                'disabled' => true,
            ])
            ->add('save', SubmitType::class, ['label' => 'Créer l\'évènement']);
    } 

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Events::class,
        ]);
    }
}
