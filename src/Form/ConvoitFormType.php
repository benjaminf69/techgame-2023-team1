<?php

namespace App\Form;

use App\Entity\Posts;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ConvoitFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('id', HiddenType::class, [
            'required' => false, // Rend le champ facultatif
            'mapped' => false, // Ne mappe pas directement à l'entité
        ])
        ->add('title', TextType::class)
        ->add('content', TextType::class)
        ->add('save', SubmitType::class, ['label' => 'Créer le trajet'])
        ->add('user', TextType::class, [
            'disabled' => true, 
        ])
        ->add('category', TextType::class, [
            'disabled' => true,
        ])
        ->add('imageFile', FileType::class, [
            'label' => 'Image (JPEG, PNG, GIF)',
            'required' => false,
            'mapped' => false, // Ne pas mapper directement à l'entité
        ])
    ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Posts::class,
        ]);
    }
}
