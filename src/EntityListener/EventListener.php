<?php

namespace App\EntityListener;

use Doctrine\Persistence\Event\LifecycleEventArgs;
use App\Entity\Events;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use App\Repository\UserRepository;
use App\EntityListener\Response;
use Psr\Log\LoggerInterface;



class EventListener
{
    private $logger;
    private $userRepository;
    private $mailer;


    public function __construct(UserRepository $userRepository, LoggerInterface $logger, MailerInterface $mailer)
    {
        $this->logger = $logger;
        $this->userRepository = $userRepository;
        $this->mailer = $mailer;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if ($entity instanceof Events) {
            $subject = 'Nouvel évènement G4 !';
            $users = $this->userRepository->findAll(); // Méthode à implémenter selon vos besoins
            $content = 'Un nouvel évènement à été ajouté : ' . $entity->getName();

        // A décommenter lors de la démo du à la limitation de 50 mail/h
        //     foreach ($users as $user) {
        //         $email = (new Email())
        //             ->from('mailtrap@cineflash.net')
        //             ->to($user->getEmail())
        //             ->subject($subject)
        //             ->text($content);
        //         try {
        //             $this->mailer->send($email);
        //         } catch (\Throwable $th) {
        //             $this->logger->error($th);
        //         }
        //     }
        }
    }
}
