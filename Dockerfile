# Dockerfile.websocket
FROM php:8.1.1-cli

# Installer git, unzip, et l'extension zip pour PHP
RUN apt-get update && apt-get install -y git unzip libzip-dev \
    && docker-php-ext-install zip sockets

# Définir le répertoire de travail
WORKDIR /app

# Copier l'ensemble du projet dans le conteneur
# Cela inclut le dossier vendor et toutes les autres dépendances
COPY . /app

# Exposer le port utilisé par le serveur WebSocket
EXPOSE 8081

# Lancer le serveur WebSocket
CMD ["php", "websocket.php"]
