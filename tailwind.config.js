/** @type {import('tailwindcss').Config} */

const colors = require('tailwindcss/colors')

module.exports = {
  content: [
    "./assets/**/*.js",
    "./templates/**/*.html.twig",
  ],
  theme: {
    extend: {
      fontFamily: {
        'montserrat': ['Montserrat']
      }
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: '#f8f8f8',
      gray: colors.trueGray,
      indigo: colors.indigo,
      red: colors.rose,
      yellow: colors.amber,
      primary: "#4f4f9f",
      secondary: "#5d5dea",
      beige: '#F9EDED',
      strongBeige: '#F0D1D1',
      lime: '#00FF00',
      gris: '#5F5F5F',
    },
  },
  plugins: [],
}