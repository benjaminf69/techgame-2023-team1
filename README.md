# Projet Symfony Réseau Social

Ce projet est un réseau social développé avec Symfony, destiné aux étudiants de l'Institut G4. Il comprend des fonctionnalités telles qu'un calendrier interactif, une messagerie instantanée, un système de catégories, et bien plus.

## Installation

Avant de démarrer, assurez-vous d'avoir installé PHP, Composer, Node.js, et Yarn (ou npm) sur votre système.

### Étapes d'Installation

1. **Clonez le Répertoire**

        git clone https://gitlab.com/benjaminf69/techgame-2023-team1.git

2. **Installez les Dépendances**
    - Installez les dépendances PHP :
        ```sh
        composer install
    - Installez les dépendances JavaScript :
        ```sh
        yarn install
    - Ou si vous utilisez npm :
        ```sh
        npm install
3. **Démarrez le Serveur**
    - Démarrez le serveur Symfony :
        ```sh
        symfony server:start
    - Démarrez le serveur Webpack Encore :
        ```sh
        yarn run dev-server
    - Ou si vous utilisez npm :
        ```sh
        npm run dev-server
      
## Fonctionnalités

- **Calendrier Interactif** : Gestion d'événements avec notifications.
- **Messagerie Instantanée** : Communication en temps réel entre utilisateurs.
- **Gestion de Catégories** : Diverses catégories pour les publications et événements.
- **Design Personnalisé** : Interface utilisateur adaptée à l'identité de l'Institut G4.
- **Expérience Utilisateur Optimisée** : Navigation intuitive et conviviale.

# Guide Simplifié de Symfony

Ce document fournit un aperçu rapide des concepts clés de Symfony pour aider les nouveaux utilisateurs à se familiariser avec le framework.

## Structure de Base

- **`bin/`**: Contient les scripts exécutables, y compris `console` pour exécuter les commandes Symfony.
- **`config/`**: Dossiers et fichiers de configuration.
- **`public/`**: Dossier racine du web, contient le fichier `index.php`.
- **`src/`**: Code source principal de votre application.
- **`templates/`**: Fichiers de templates Twig.
- **`vendor/`**: Bibliothèques tierces, ne pas modifier.

## Concepts Clés

### 1. **Contrôleur (Controller)**
- Gère les requêtes HTTP.
- Utilisez `php bin/console make:controller` pour générer un nouveau contrôleur.
- Les méthodes du contrôleur retournent un objet `Response`.

### 2. **Routeur (Routing)**
- Définit comment les requêtes HTTP sont mappées aux contrôleurs.
- Les routes peuvent être configurées via annotations dans les contrôleurs ou dans des fichiers YAML/XML/PHP.

### 3. **Entité (Entity)**
- Représente une table dans votre base de données.
- Utilisez `php bin/console make:entity` pour créer une nouvelle entité.
- 
### 3.5. **Repository**
- Utilisé pour encapsuler la logique d'accès aux données pour une entité.
- Fournit des méthodes pour interroger la base de données, souvent via Doctrine Query Language (DQL).
- Créez un repository personnalisé avec `php bin/console make:repository`.

### 4. **Doctrine ORM**
- Gère la persistance des données.
- Utilisez `php bin/console make:migration` et `php bin/console doctrine:migrations:migrate` pour gérer les migrations de base de données.

### 5. **Service**
- Un "service" est tout objet qui fait quelque chose d'utile (logique métier, accès aux données, etc.).
- Les services sont souvent configurés et récupérés depuis le conteneur de services.

### 6. **Twig**
- Moteur de template pour créer des vues.
- Syntaxe simple et extensible.

### 6. **Repository**
- Utilisé pour encapsuler la logique d'accès aux données pour une entité.
- Fournit des méthodes pour interroger la base de données, souvent via Doctrine Query Language (DQL).
- Créez un repository personnalisé avec `php bin/console make:repository`.
### 7. **Formulaires**
- Générez et traitez des formulaires.
- Utilisez `php bin/console make:form` pour créer un type de formulaire.

### 8. **Validation**
- Assurez la validité des données.
- Annotations dans les entités ou dans le type de formulaire.

### 9. **Sécurité**
- Gestion des utilisateurs, authentification, autorisation.
- `User`, `Firewall`, `Access Control` sont des concepts clés.

### 10. **Console**
- Commandes pour effectuer des tâches côté serveur.
- Utilisez `php bin/console` suivi de la commande souhaitée.

## Ressources Utiles
- **Documentation Officielle** : [Symfony Documentation](https://symfony.com/doc/current/index.html)
- **Communauté Symfony** : Forums, Slack, et Stack Overflow pour obtenir de l'aide.

## Conclusion

Symfony est un puissant framework qui offre une structure et des outils pour construire des applications web robustes. Ce guide offre un aperçu de ses fonctionnalités principales pour un démarrage rapide.
